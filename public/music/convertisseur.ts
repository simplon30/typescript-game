import { myosotisEZ } from "./Mysosotis/myosotisEZ"
import { clearMyosotisEZ } from "./Mysosotis/myosotisEZ";

import { zenEZ } from "./Zen/ZenEZ"
import { clearZenEZ } from "./Zen/ZenEZ";

import { brynhildrInTheDarkness } from "./Brynhildr/brynhildrInTheDarkness";
import { clearBrynhildrInTheDarkness } from "./Brynhildr/brynhildrInTheDarkness";

// [HitObjects]
// col,jsp,tempo,jsp,jsp,noteLongue:jsp:duréenote

interface song {
    col: number,
    tempo: number,
}

let test = [
    64, 192, 6, 5, 0,
    192, 192, 85, 1, 0,
    320, 192, 164, 1, 0,
    448, 192, 243, 1,

    64, 64, 192, 321, 1, 0,
    192, 192, 321, 1, 0,
    320, 192, 321, 1, 0,
    448, 192, 321, 1, 0,
]

let pouet = [
    //collone + tempo à afficher
    { "col" : 64, "tempo": 100 },
    { "col": 192, "tempo": 200 },
    { "col": 320, "tempo": 300 },
    { "col": 448, "tempo": 400 },

    { "col": 448, "tempo": 600 },
    { "col": 320, "tempo": 600 },
    { "col": 192, "tempo": 600 },
    { "col": 64, "tempo": 600 },
];


let Music: { col: number, tempo: number }[] = [];

let music = [{ col: 64, tempo: 6, },]

let jsonData = JSON.stringify(Music);

let col = test[0]
let tempo = test[2]

colAndTempoFinder(test)


//pour afficher toute les notes dans un Tableau d'objet
// console.log(JSON.stringify(Music));

/**
 * convertis un tableau de note Osu!Mania en ce que je souhaite
 * @param tabSong donner un tableau de note
 */
function colAndTempoFinder(tabSong: number[]) {

    for (let i = 0; i < tabSong.length; i += 5) {

        Music.push({
            col: tabSong[i],
            tempo: tabSong[i + 2]

        });

    }
}
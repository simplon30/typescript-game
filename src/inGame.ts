//console.log("in Game");

//tableau de notes
import { clearZenEZ } from "../public/music/Zen/ZenEZ";
import { clearMyosotisEZ, myosotisEZ } from "../public/music/Mysosotis/myosotisEZ";
import { clearBrynhildrInTheDarkness } from "../public/music/Brynhildr/brynhildrInTheDarkness";

//images
import kuroha from "../public/img/kuroha.jpg";
import zenImg from "../public/img/zenzenzen.jpg";
import myosotis from "../public/img/myosotis.jpg";

//selectionne score et combo
const scoreIG = document.querySelector("#scoreIG");
const comboIG = document.querySelector("#comboIG");

//selectionne son qui est dans le html
const hitSound = document.querySelector("#hitSound") as HTMLAudioElement;
const sectionPass = document.querySelector("#sectionPass") as HTMLAudioElement
const menuClick = document.querySelector("#menuClick") as HTMLAudioElement
const menuHit = document.querySelector("#menuHit") as HTMLAudioElement
const menuBack = document.querySelector("#menuBack") as HTMLAudioElement

//les musiques
const Myosotis = document.querySelector("#Myosotis") as HTMLAudioElement
const brynhildr = document.querySelector("#BrynhildrInTheDarkness") as HTMLAudioElement
const zen = document.querySelector("#zen") as HTMLAudioElement

//les boutons
const btn = document.querySelectorAll(".btn")
const btnModal = document.querySelectorAll(".btnModal")
const btnReplay = document.querySelector("#btnReplay")

//les boutons choix de la musique
const btnMusic1 = document.querySelector('.btnMusic1');
const btnMusic2 = document.querySelector('.btnMusic2');
const btnMusic3 = document.querySelector('.btnMusic3');

//les notes
const noteD = document.querySelector(".noteD");
const noteF = document.querySelector(".noteF");
const noteJ = document.querySelector(".noteJ");
const noteK = document.querySelector(".noteK");

//les colonnes
const colonneD = document.querySelector<HTMLElement>(".colonneD")
const colonneF = document.querySelector<HTMLElement>(".colonneF")
const colonneJ = document.querySelector<HTMLElement>(".colonneJ")
const colonneK = document.querySelector<HTMLElement>(".colonneK")

// const col1 = document.querySelector(".col1")
// const col2 = document.querySelector(".col2")
// const col3 = document.querySelector(".col3")
// const col4 = document.querySelector(".col4")

//zone au dessus des notes
const fRectD = document.querySelector(".fRectD");
const fRectF = document.querySelector(".fRectF");
const fRectJ = document.querySelector(".fRectJ");
const fRectK = document.querySelector(".fRectK");

//img hit or miss
const imgHit = document.querySelector(".imgHit");
const imgMiss = document.querySelector(".imgMiss");

//la zone blanche ou faut hit la note
const hitNoteElement = document.querySelector(".hitNote");


//les sections
const section1 = document.querySelector(".section1");
const section2 = document.querySelector(".section2");
const sectionIG = document.querySelector(".sectionIG");
const sectionRecap = document.querySelector(".sectionRecap");

//ecran de fin de partie
const scoreR = document.querySelector("#scoreR");
const musicR = document.querySelector("#musicR");
const scoreM = document.querySelector("#scoreM");
const listeScore = document.querySelector(".listeScore");

//pour afficher video
const body = document.querySelector<HTMLBodyElement>("#body");
const myVideo = document.querySelector<HTMLVideoElement>("#myVideo")

//pour stocker le score
const newLocal = localStorage.getItem('score');
let tab: string[] = newLocal ? JSON.parse(newLocal) : []

//note hit
let score = 0
//not miss
let scoreMiss = 0

//empecher la triche 😈
const keysPressed = {};
let keyPressTimestamps = {
  d: false,
  f: false,
  j: false,
  k: false,
};

//vitesse défilement des notes
let tempo = 0
let actualSong = ""

//bool pour stop le jeu
let cancel = true

//bar de progression
let progressBar = document.querySelector<HTMLProgressElement>("#progressBar");
let maxSet = false;

let pouet = [
  //collone + tempo à afficher
  { "col": 64, "tempo": 100 },
  { "col": 192, "tempo": 600 },
  { "col": 320, "tempo": 1100 },
  { "col": 448, "tempo": 1600 },

  { "col": 64, "tempo": 2500 },
  { "col": 192, "tempo": 3000 },
  { "col": 320, "tempo": 3500 },
  { "col": 448, "tempo": 4000 },

  { "col": 64, "tempo": 5000 },
  { "col": 320, "tempo": 5000 },
  { "col": 192, "tempo": 6000 },
  { "col": 448, "tempo": 6000 },

  { "col": 448, "tempo": 7000 },
  { "col": 320, "tempo": 7500 },
  { "col": 192, "tempo": 8000 },
  { "col": 64, "tempo": 8500 },
  { "col": 192, "tempo": 9000 },
  { "col": 320, "tempo": 9500 },
  { "col": 448, "tempo": 10000 },

  { "col": 192, "tempo": 11000 },
  { "col": 320, "tempo": 12000 },
  { "col": 64, "tempo": 13000 },
  { "col": 448, "tempo": 13000 },

  { "col": 192, "tempo": 14000 },
  { "col": 320, "tempo": 14500 },
  { "col": 192, "tempo": 15000 },
  { "col": 320, "tempo": 15500 },
  { "col": 192, "tempo": 16000 },
  { "col": 320, "tempo": 16500 },
  { "col": 64, "tempo": 17000 },
  { "col": 448, "tempo": 17000 },

  { "col": 192, "tempo": 18000 },
  { "col": 320, "tempo": 18500 },
  { "col": 64, "tempo": 19000 },
  { "col": 448, "tempo": 19000 },

  { "col": 64, "tempo": 20000 },
  { "col": 192, "tempo": 20000 },

  { "col": 320, "tempo": 21000 },
  { "col": 448, "tempo": 21000 },

  { "col": 64, "tempo": 22000 },
  { "col": 192, "tempo": 22000 },
  { "col": 320, "tempo": 22000 },
  { "col": 448, "tempo": 22000 },

  { "col": 64, "tempo": 23000 },
  { "col": 192, "tempo": 23000 },
  { "col": 320, "tempo": 23000 },
  { "col": 448, "tempo": 23000 },

  { "col": 64, "tempo": 24000 },
  { "col": 192, "tempo": 24500 },
  { "col": 64, "tempo": 24500 },
  { "col": 64, "tempo": 25000 },
  { "col": 320, "tempo": 25000 },
  { "col": 448, "tempo": 25500 },
  { "col": 64, "tempo": 25500 },
  { "col": 320, "tempo": 26000 },
  { "col": 64, "tempo": 26000 },
  { "col": 192, "tempo": 26500 },
  { "col": 64, "tempo": 26500 },
  { "col": 64, "tempo": 27000 },

  { "col": 192, "tempo": 28000 },
  { "col": 448, "tempo": 28500 },
  { "col": 64, "tempo": 29000 },
  { "col": 320, "tempo": 29500 },
  { "col": 320, "tempo": 30000 },
  { "col": 320, "tempo": 30500 },








];


//joue un son pour chaque bouton
for (let index = 0; index < btn.length; index++) {

  btn[index].addEventListener("mouseover", function () {
    menuClick.play();
  })

  btn[index].addEventListener("click", function () {
    menuHit.play();
  })
}

//joue un son pour chaque bouton
for (let index = 0; index < btnModal.length; index++) {

  btnModal[index].addEventListener("click", function () {
    menuBack.play();
  })

}

//joue un son pour chaque bouton
btnReplay?.addEventListener("click", function () {
  location.reload()
})


//affiche le score en fonction du local storage
for (let index = 0; index < tab.length; index++) {

  const li = document.createElement("li")
  li.textContent = `${tab[index]} : ${tab[index + 1]}`
  listeScore?.appendChild(li)
  index++;

}

/**
 * Bouton musique Brynhildr
 * affiche la video
 * appel la fonction qui crée les notes
 * lance l'audio de la musique
 * */
btnMusic1?.addEventListener("click", function () {

  myVideo?.classList.remove("displayNone")

  if (body) {
    body.style.backgroundImage = `url(${kuroha})`
  }
  section2?.classList.add("displayNone")
  sectionIG?.classList.remove("displayNone")
  createNoteWithDelay(0, clearBrynhildrInTheDarkness)
  actualSong = "Brynhildr in the Darkness"
  setTimeout(() => {
    brynhildr.play()
  }, 1500)

});

/**
 * Bouton musique Zen
 * change l'image de fond
 * appel la fonction qui crée les notes
 * lance l'audio de la musique
 * */
btnMusic2?.addEventListener("click", function () {

  if (body) {
    body.style.backgroundImage = `url(${zenImg})`
  }

  section2?.classList.add("displayNone")
  sectionIG?.classList.remove("displayNone")
  actualSong = "Zen Zen Zense"
  createNoteWithDelay(0, pouet)
  setTimeout(() => {
    zen.play()
  }, 1500)

});

/**
 * Bouton musique Myosotis
 * change l'image de fond
 * appel la fonction qui crée les notes
 * lance l'audio de la musique
 * */
btnMusic3?.addEventListener("click", function () {

  if (body) {
    body.style.backgroundImage = `url(${myosotis})`
  }

  section2?.classList.add("displayNone")
  sectionIG?.classList.remove("displayNone")
  actualSong = "Myosotis"
  createNoteWithDelay(0, clearMyosotisEZ)
  setTimeout(() => {
    Myosotis.play()
  }, 1500)

});

//event sur les touches DF JK
document.addEventListener('keydown', (event) => {

  //La variable keysPressed est un dictionnaire qui stocke l'état de chaque touche du clavier. 
  //Si la touche n'a pas encore été enfoncée (c'est-à-dire si keysPressed[event.key] est false), 
  //le score est mis à jour et la variable keysPressed est mise à jour pour indiquer que la touche a été enfoncée. 
  //Si la touche a déjà été enfoncée (c'est-à-dire si keysPressed[event.key] est true), le score n'est pas mis à jour.

  //Lorsque l'utilisateur relâche la touche (c'est-à-dire lorsque l'événement keyup est déclenché), 
  //la variable keysPressed est mise à jour pour indiquer que la touche n'est plus enfoncée.
  if (!keysPressed[event.key]) {

    keysPressed[event.key] = true;

    //si on appuye sur DFJK le score s'incrémente
    if (event.key === 'd' || event.key === 'f' || event.key === 'j' || event.key === 'k') {

      hitSound.currentTime = 0; // Remettre le temps du son à 0 pour pouvoir le jouer à nouveau
      hitSound.play();

      if (event.key === 'd') {
        keyPressTimestamps.d = true
        fRectD?.classList.toggle("displayNone");
      }
      if (event.key === 'f') {

        keyPressTimestamps.f = true
        fRectF?.classList.toggle("displayNone");
      }
      if (event.key === 'j') {

        keyPressTimestamps.j = true
        fRectJ?.classList.toggle("displayNone");
      }
      if (event.key === 'k') {

        keyPressTimestamps.k = true
        fRectK?.classList.toggle("displayNone");
      }
    }
  }
});

//Il utilise également l'événement keyup pour détecter lorsque l'utilisateur relâche une touche du clavier et
// met à jour la variable keysPressed en conséquence.
document.addEventListener("keyup", (event) => {
  keysPressed[event.key] = false;

  //masque la zone blanche
  fRectD?.classList.add("displayNone");
  fRectF?.classList.add("displayNone");
  fRectJ?.classList.add("displayNone");
  fRectK?.classList.add("displayNone");

});

updateNotePosition()

/**
 * Fonction qui vérifie l'état des notes en continu
 * si la note est sur la zone de frappe et que le joueur appuie sur la bonne touche alors le score augmente
 * si la note sort de l'écran alors miss s'incrémente 
 * affiche hit or miss en fonction
 */
function updateNotePosition() {

  const noteD = document.querySelector(".noteD");
  const noteF = document.querySelector(".noteF");
  const noteJ = document.querySelector(".noteJ");
  const noteK = document.querySelector(".noteK");

  const hitNoteTop = (hitNoteElement as HTMLElement).offsetTop;
  const hitNoteBottom = hitNoteTop + (hitNoteElement as HTMLElement).offsetHeight;

  //si la noteD existe alors
  if (noteD) {

    const noteTopD = (noteD as HTMLElement).getBoundingClientRect().top;

    if (noteTopD >= hitNoteTop && noteTopD <= hitNoteBottom) {
      //console.log("La note D est sur la zone de frappe.");

      if (keyPressTimestamps.d) {
        score++;

        //console.log('D GG !');

        imgHit?.classList.remove("displayNone")
        setTimeout(() => {
          imgHit?.classList.add("displayNone")
        }, 200)

        keyPressTimestamps.d = false;

        (noteD.parentNode as Node).removeChild(noteD);
      }
    } else if (noteTopD < hitNoteTop) {

      keyPressTimestamps.d = false
      //console.log("La note est au-dessus de la zone de frappe.");

    } else if (noteTopD > 820) {

      //console.log('miss');
      scoreMiss = scoreMiss + 1;

      // colonneD?.classList.remove("animation")      

      imgMiss?.classList.remove("displayNone");
      setTimeout(() => {
        imgMiss?.classList.add("displayNone");
      }, 200)

    } else {
      keyPressTimestamps.d = false
    }

  }

  //si la noteD existe alors
  if (noteF) {

    const noteTopF = (noteF as HTMLElement).getBoundingClientRect().top;

    if (noteTopF >= hitNoteTop && noteTopF <= hitNoteBottom) {
      //console.log("La note D est sur la zone de frappe.");

      if (keyPressTimestamps.f) {
        score++;

        //console.log('F GG !');

        imgHit?.classList.remove("displayNone")
        setTimeout(() => {
          imgHit?.classList.add("displayNone")
        }, 200)

        keyPressTimestamps.f = false;
        (noteF.parentNode as Node).removeChild(noteF);
      }
    } else if (noteTopF < hitNoteTop) {

      keyPressTimestamps.f = false
      //console.log("La note est au-dessus de la zone de frappe.");

    } else if (noteTopF > 820) {
      scoreMiss = scoreMiss + 1;


      imgMiss?.classList.remove("displayNone");
      setTimeout(() => {
        imgMiss?.classList.add("displayNone");
      }, 200)

      // colonneF?.classList.remove("animation")      


    } else {
      keyPressTimestamps.f = false
      //console.log("La note est en dessous de la zone de frappe.");
    }

  }
  // const noteTopF = (noteF as HTMLElement).getBoundingClientRect().top;



  if (noteJ) {

    const noteTopJ = (noteJ as HTMLElement).getBoundingClientRect().top;

    if (noteTopJ >= hitNoteTop && noteTopJ <= hitNoteBottom) {
      // console.log("La note J est sur la zone de frappe.");

      if (keyPressTimestamps.j) {
        score++;

        //console.log('J GG !');
        keyPressTimestamps.j = false;
        imgHit?.classList.remove("displayNone")
        setTimeout(() => {
          imgHit?.classList.add("displayNone")
        }, 200);
        (noteJ.parentNode as Node).removeChild(noteJ);
      }
    } else if (noteTopJ < hitNoteTop) {

      keyPressTimestamps.j = false
      //console.log("La note est au-dessus de la zone de frappe.");
    } else if (noteTopJ > 820) {

      imgMiss?.classList.remove("displayNone");
      setTimeout(() => {
        imgMiss?.classList.add("displayNone");
      }, 200);

      // colonneJ?.classList.remove("animation")      


    } else {
      keyPressTimestamps.j = false
      //console.log("La note est en dessous de la zone de frappe.");
    }

  }

  if (noteK) {

    const noteTopK = (noteK as HTMLElement).getBoundingClientRect().top;

    if (noteTopK >= hitNoteTop && noteTopK <= hitNoteBottom) {
      //console.log("La note F est sur la zone de frappe.");

      if (keyPressTimestamps.k) {
        score++;

        //console.log('K GG !');

        keyPressTimestamps.k = false

        imgHit?.classList.remove("displayNone")
        setTimeout(() => {
          imgHit?.classList.add("displayNone")
        }, 200);
        (noteK.parentNode as Node).removeChild(noteK);
      }
    } else if (noteTopK < hitNoteTop) {

      keyPressTimestamps.k = false
      //console.log("La note est au-dessus de la zone de frappe.");
    } else if (noteTopK > 820) {

      scoreMiss = scoreMiss + 1;


      imgMiss?.classList.remove("displayNone");
      setTimeout(() => {
        imgMiss?.classList.add("displayNone");
      }, 200);

      // colonneK?.classList.remove("animation")      

    } else {
      keyPressTimestamps.k = false
      //console.log("La note est en dessous de la zone de frappe.");
    }


  }

  if (scoreIG) {
    scoreIG.innerHTML = score.toString();
  }

  requestAnimationFrame(updateNotePosition)

}

/**
 * fonction qui parcour le tableau de note et crée chaque note après un delay
 * @param index mettre 0
 * @returns
*/
export function createNoteWithDelay(index, song) {

  setTimeout(() => {
    createNewNote(song);
    //vitesse des notes
    tempo = tempo + 6  

    if (cancel == true) {

      createNoteWithDelay(index + 1, song);

      if (progressBar) {
        progressBar.value = song.length

        updateProgressBar()

        /**
         * Actualise la bar de progession en fonction des notes restantes
         */
        function updateProgressBar() {
          if (!maxSet && progressBar) {
            progressBar.max = song.length;
            maxSet = true;
          }
          if (progressBar) {
            progressBar.value = song.length;
          }
        }
      }

    } else {
      //une fois le tableau de note fini
      setTimeout(() => {

        sectionPass.play()
        sectionIG?.classList.toggle("displayNone")
        sectionRecap?.classList.toggle("displayNone")

        //sauvegarde le score
        tab.push(actualSong, String(score))
        localStorage.setItem("score", JSON.stringify(tab))

        if (scoreR) {
          scoreR.innerHTML = score.toString()
        }

        if (musicR) {
          musicR.innerHTML = `Musique : ${actualSong}`
        }
        
        if (scoreM) {
          scoreM.innerHTML = parseInt( (scoreMiss / 3).toString())
        } 

      }, 3000);
    }
  }, 3);

};

/**
 * Crée une note
 * @param classNote tabNote[index].class : noteD
 * @param col tabNote[index].col : .colonneD
 */
function createNewNote(myTab) {

  for (let index = 0; index < myTab.length; index++) {

    //marge pour créer une note en fonction du tempo
    if (tempo >= myTab[index].tempo - 10 && tempo <= myTab[index].tempo + 10) {

      const newNote = document.createElement("div");
      //une fois l'animation fini remove la div
      newNote.addEventListener("animationend", (event) => {
        newNote.remove()
      })

      if (myTab[index].col === 64) {
        colonneD?.appendChild(newNote)
        newNote.classList.add("noteD")
        myTab.splice(index, 1)

      } else if (myTab[index].col === 192) {
        colonneF?.appendChild(newNote)
        newNote.classList.add("noteF")
        myTab.splice(index, 1)

      } else if (myTab[index].col === 320) {
        colonneJ?.appendChild(newNote)
        newNote.classList.add("noteJ")
        myTab.splice(index, 1)

      } else if (myTab[index].col === 448) {
        colonneK?.appendChild(newNote)
        newNote.classList.add("noteK")
        myTab.splice(index, 1)
      }
    } if (myTab.length === 0) {
      cancel = false
    }

  }

};


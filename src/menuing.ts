//les boutons
const btnJouer = document.querySelector("#btnJouer");
const btnInstruction = document.querySelector("#btnInstruction");
const btnScore = document.querySelector("#btnScore");
const btnScoreInsideScore = document.querySelector(".btnScoreInsideScore");
const btnMusic1 = document.querySelector('.btnMusic1');
const btnDif = document.querySelectorAll('.btnDif');

//modal
const myModal = document.querySelector('#myModal');
const btnModal = document.querySelector(".btnModal");

//sections
const section1 = document.querySelector(".section1");
const section2 = document.querySelector(".section2");
const section3 = document.querySelector(".section3");
const sectionScore = document.querySelector(".sectionScore");
const sectionIG = document.querySelector(".sectionIG");

//affiche le score
btnScore?.addEventListener("click", (event) => {
    sectionScore?.classList.remove("displayNone")
    section1?.classList.add("displayNone")
});

//masque le score
btnScoreInsideScore?.addEventListener('click', (event) => {
    sectionScore?.classList.add("displayNone")
    section1?.classList.remove("displayNone")
});

//passe de jouer aux choix des musiques
btnJouer?.addEventListener('click', (event) => {
    section1?.classList.add("displayNone")
    section2?.classList.remove("displayNone")
});

//cache la modal avec le X
btnModal?.addEventListener("click", (event) => {
    myModal?.classList.toggle('displayYes')
});

//affiche la modal quand instruction est cliqué
btnInstruction?.addEventListener('click', (event) => {
    myModal?.classList.toggle('displayYes')
});

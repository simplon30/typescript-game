# Projet Quiz

## Presentation

Bonjour ! Je vais vous présenter mon jeu.  
Le projet est réalisé dans le cadre de la formation développeur web et web mobile de Simplon.
C'est le troisième projet que je réalise dans ce cadre.

J'ai utilisé les langages suivants : 
- HTML
- CSS
- Typescript

J'ai aussi utilité Parcel.

Le but est de faire un jeu vidéo qui se rapproche du jeu Osu!mania.
- le but du jeu est que le joueur devra appuyer sur la touche correspondant à la note au bon moment.
- le score sera incrémenté si le joueur réussi
- à la fin il y a un écran récapitulatif
- chaque score est stocké en localstorage

J'avais envie de refaire un jeu que j'aime et c'était un sacré un challenge !

J'ai commenté mon code et mes fonctions Typescript. 

## wireframe + Maquettes

https://www.figma.com/file/pPKkibRpJDG28Z1uTtmxyf/ts-game?node-id=0%3A1&t=rgVGc4nccJ7npxn4-1

## Amélioration Futur

- Plus d'animation !
- Une synchronisation parfaite entre les notes et la musique
- Plus de Musique !
- Choix de difficulté
- système de combo
- Faire le responsive